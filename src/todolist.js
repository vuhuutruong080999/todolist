const allowDrop = (event) => {
  event.preventDefault();
};
let taskDragID = 0;
const dragndrop = () => {
  tasks.forEach((task) => {
    const taskElement = document.getElementById(task.id);
    const iconDelete = taskElement.getElementsByClassName("fa-trash-o");
    iconDelete[0].addEventListener("click", () => {
      removeTask(task.id);
      toastChangMessage("Message", "Delete Successed!");
      $(toastMessage).toast("show");
    });
    taskElement.addEventListener("dragstart", (event) => {
      taskDragID = event.target.id;
    });
    taskElement.addEventListener("mouseenter", () => {
      const iconEdit = taskElement.getElementsByClassName("fa-edit");
      iconEdit[0].hidden = false;
      iconEdit[0].addEventListener("click", () => {
        modalChangeMessage(`Edit`, getEditTask(task.title));
        $(`#detail`).modal("show");
        editTaskModal(task.id);
      });
    });
    taskElement.addEventListener("mouseleave", () => {
      const iconEdit = taskElement.getElementsByClassName("fa-edit");
      iconEdit[0].hidden = true;
    });
    taskElement.addEventListener("click", () => {
      $(`#detail`).modal("show");
      modalChangeDetail("Detail", getTaskDetailUI(task.id, task.imageSrc));
      changeFile();
    });
  });
};
const addTask = (event) => {
  event.preventDefault;
  const tag = event.target.className;
  const taskDrag = tasks.filter((task) => task.id == taskDragID);
  if (tag.match(/table/i) == "table") {
    removeTask(taskDragID);
  }
  if (tag.match(/todo/i) == "todo") {
    taskDrag[0].status = "todo";
    todoTasks.push(taskDrag[0]);
  } else if (tag.match(/pending/i) == "pending") {
    taskDrag[0].status = "pending";
    pendingTasks.push(taskDrag[0]);
  } else if (tag.match(/complete/i) == "complete") {
    taskDrag[0].status = "complete";
    completeTasks.push(taskDrag[0]);
  } else if (tag.match(/missed/i) == "missed") {
    taskDrag[0].status = "missed";
    missedTasks.push(taskDrag[0]);
  }
  renderAll();
};
tablesAsArray.forEach((table) => {
  table.addEventListener("dragover", allowDrop);
  table.addEventListener("drop", addTask);
});

formCreate.addEventListener("submit", (submitEvent) => {
  submitEvent.preventDefault();
  if (checkID) {
    idCount++;
  }
  const task = new Task(idCount, inputTask.value, "", "todo");
  todoTasks.push(task);
  renderAll();
  inputTask.value = "";
  toastChangMessage("Message", "Create Successed!");
  $(toastMessage).toast("show");
  idCount++;
});
const checkID = () => {
  for (task of tasks) {
    if (task.id == idCount) {
      return true;
    }
  }
  return false;
};
const removeTask = (id) => {
  const afterToDoTask = todoTasks.filter((task) => task.id != id);
  todoTasks = afterToDoTask;
  const afterPendingTasks = pendingTasks.filter((task) => task.id != id);
  pendingTasks = afterPendingTasks;
  const afterCompleteTasks = completeTasks.filter((task) => task.id != id);
  completeTasks = afterCompleteTasks;
  const afterMissedTasks = missedTasks.filter((task) => task.id != id);
  missedTasks = afterMissedTasks;
  renderAll();
};
const renderToDoList = () => {
  const taskUIs = todoTasks.map(getTaskUI);
  listGroup[0].innerHTML = taskUIs.join("");
  saveData();
};
const renderPendingList = () => {
  const taskUIs = pendingTasks.map(getTaskUI);
  listGroup[1].innerHTML = taskUIs.join("");
  saveData();
};
const renderCompleteList = () => {
  const taskUIs = completeTasks.map(getTaskUI);
  listGroup[2].innerHTML = taskUIs.join("");
  saveData();
};
const renderMissedList = () => {
  const taskUIs = missedTasks.map(getTaskUI);
  listGroup[3].innerHTML = taskUIs.join("");
  saveData();
};
const renderAll = () => {
  renderToDoList();
  renderCompleteList();
  renderPendingList();
  renderMissedList();
  tasks = [];
  for (task of todoTasks) {
    tasks.push(task);
  }
  for (task of pendingTasks) {
    tasks.push(task);
  }
  for (task of completeTasks) {
    tasks.push(task);
  }
  for (task of missedTasks) {
    tasks.push(task);
  }
  saveData();
  dragndrop();
};
const editTitleTask = (id, title) => {
  const afterToDoTask = todoTasks.map((task) => {
    if (task.id === id) {
      task.title = title;
    }
    return task;
  });
  todoTasks = afterToDoTask;
  const afterPendingTasks = pendingTasks.map((task) => {
    if (task.id === id) {
      task.title = title;
    }
    return task;
  });
  pendingTasks = afterPendingTasks;
  const afterCompleteTasks = completeTasks.map((task) => {
    if (task.id === id) {
      task.title = title;
    }
    return task;
  });
  completeTasks = afterCompleteTasks;
  const afterMissedTasks = missedTasks.map((task) => {
    if (task.id === id) {
      task.title = title;
    }
    return task;
  });
  missedTasks = afterMissedTasks;
  renderAll();
};
const editImageSrc = (id, imageSrc) => {
  const afterToDoTask = todoTasks.map((task) => {
    if (task.id === id) {
      task.imageSrc = imageSrc;
    }
    return task;
  });
  todoTasks = afterToDoTask;
  const afterPendingTasks = pendingTasks.map((task) => {
    if (task.id === id) {
      task.imageSrc = imageSrc;
    }
    return task;
  });
  pendingTasks = afterPendingTasks;
  const afterCompleteTasks = completeTasks.map((task) => {
    if (task.id === id) {
      task.imageSrc = imageSrc;
    }
    return task;
  });
  completeTasks = afterCompleteTasks;
  const afterMissedTasks = missedTasks.map((task) => {
    if (task.id === id) {
      task.imageSrc = imageSrc;
    }
    return task;
  });
  missedTasks = afterMissedTasks;
  renderAll();
};
const editTaskModal = (id) => {
  $("#formEditTask").submit((event) => {
    event.preventDefault();
    editTitleTask(id, $("#inputEditTask").val());
    toastChangMessage("Message", "Edit Successed!");
    $(toastMessage).toast("show");
  });
};
const modalChangeDetail = (label, content) => {
  $(`#detailLabel`).text(label);
  modal[0].innerHTML = `${content}`;
};
const toastChangMessage = (title, message) => {
  $(`#titleToast`).text(title);
  $(`#contentToast`).text(message);
};
