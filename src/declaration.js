const toDoList = document.getElementById("to-do-list");
const tables = toDoList.getElementsByClassName("table");
const listGroup = toDoList.getElementsByClassName("list-group");
const tablesAsArray = Array.from(tables);
const formCreate = document.getElementById("createItem");
const inputTask = formCreate["createTask"];
const modal = document.getElementsByClassName("modal-body");
let todoTasks = [];
let pendingTasks = [];
let completeTasks = [];
let missedTasks = [];
let tasks = [];
let idCount = 1;
class Task {
  id;
  title;
  imageSrc;
  status;
  constructor(id, title, imageSrc, status) {
    this.id = id;
    this.title = title;
    this.imageSrc = imageSrc;
    this.status = status;
  }
}
function getTaskDetailUI(id, imageSrc) {
  return `<div class="d-flex flex-wrap align-items-start" id="uploadByFile">
                <div class="custom-file mb-3">
                  <input type="file" class="custom-file-input col-1" id="photo" name="filename"/>
                  <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                <button class="btn btn-success" type="button" onclick="uploadImage(${id})">Upload</button>
                <div class="col-6">
                  <img class="w-100" id="image-load" src="${imageSrc}" alt="">
                </div>
              </div>`;
}
function getTaskUI(task) {
  return `<li class="list-group-item d-flex justify-content-between mb-2" draggable="true" id="${task.id}"><span>${task.title}</span>
  <div>
    <i class="fa fa-edit" onclick="event.stopPropagation()" hidden="true"></i>
    <i class="fa fa-trash-o" onclick="event.stopPropagation()"></i>
  </div></li>`;
}
function getModalEdit(title) {
  return `<form id="formEditTask">
          <label for="">Title</label>
          <input type="text"
          class="form-control" name="editTask" id="inputEditTask" value="${title}">
          </form>`;
}
