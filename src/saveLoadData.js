const saveData = () => {
  localStorage.setItem("todoTasks", JSON.stringify(todoTasks));
  localStorage.setItem("pendingTasks", JSON.stringify(pendingTasks));
  localStorage.setItem("completeTasks", JSON.stringify(completeTasks));
  localStorage.setItem("idCount", JSON.stringify(idCount));
};
const loadData = () => {
  const todoTasksAsString = localStorage.getItem("todoTasks");
  const pendingTasksAsString = localStorage.getItem("pendingTasks");
  const completeTasksAsString = localStorage.getItem("completeTasks");
  const missedTasksAsString = localStorage.getItem("missedTasks");
  todoTasks = JSON.parse(todoTasksAsString) ?? [];
  pendingTasks = JSON.parse(pendingTasksAsString) ?? [];
  completeTasks = JSON.parse(completeTasksAsString) ?? [];
  missedTasks = JSON.parse(missedTasksAsString) ?? [];
  idCount = JSON.parse(localStorage.getItem("idCount")) ?? 1;
};
window.addEventListener("load", () => {
  loadData();
  renderAll();
});
