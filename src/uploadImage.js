function changeFile() {
  const fileLoad = document.getElementById("photo");
  const imageLoad = document.getElementById("image-load");
  fileLoad.addEventListener("change", async (event) => {
    const selectedFile = event.target.files[0];
    if (selectedFile.type === "image/png") {
      const result = await getBase64URL(selectedFile);
      imageLoad.src = result;
      $(`.custom-file-label`).text(selectedFile.name)[0];
    } else {
    }
  });
}
const getBase64URL = (selectedFile) =>
  new Promise((resolve) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      resolve(event.target.result);
    };
    reader.readAsDataURL(selectedFile);
  });

const uploadImage = (id) => {
  const ref = firebase.storage().ref();
  const file = $("#photo").prop("files")[0];
  const name = new Date() + "-" + file.name;
  const metadata = {
    contentType: file.type,
  };
  const upload = ref.child(name).put(file, metadata);
  upload
    .then((snapshot) => snapshot.ref.getDownloadURL())
    .then((url) => {
      editImageSrc(id, url);
      toastChangMessage("Message", "Upload Successed!");
      $(toastMessage).toast("show");
    });
};
